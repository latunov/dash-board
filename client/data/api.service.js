import axios from 'axios'; 

export function listLists(){
    return new Promise((resolve, reject) => {
        resolve({lists: [ {id: 1, name: "Posts"}, {id: 2, name: 'Comments'} ]});
    });
}

export function listCards(){
    return axios.get('https://jsonplaceholder.typicode.com/posts'); 
}

export function listComments(postId){
    return axios.get('https://jsonplaceholder.typicode.com/posts/' + postId + '/comments'); 
}