import { SET_CURRENT_POST, SET_SELECTED_POST } from './../constants' ;

const initialState = {
    currentPost: {}
}

export default (state = initialState, action={}) => {
    switch(action.type){
        case SET_CURRENT_POST:
            return {
                currentPost: action.post
            }
        case SET_SELECTED_POST:
            return {
                currentPost: action.post
            }
        default: return state;
    }
}
