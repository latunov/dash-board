import React, { Component } from 'react'; 
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import classnames from 'classnames'; 
import NavLinkLi from './navlink.component'; 

class NavBarComponent extends Component {
    render(){
        const userLinks = (
            <ul className="nav navbar-nav navbar-right" >
                <NavLinkLi to="/scrum" icon="trello" title="My Boards" />
            </ul>
        );

        return (
            <div className={classnames('navbar navbar-expand-sm bg-dark navbar-dark navbar-default')}>
                <div className="container-custom">
                    <div className="navbar-header">
                        <Link to="/" className='navbar-brand'><i className="fa fa-trello fa-3x" aria-hidden="true"></i> </Link>
                    </div>
                </div>

                <div className="container-custom">
                    <div className="navbar-header">
                        <Link to="/" className='navbar-brand'>Dash Board</Link>
                    </div>
                </div>

                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul className="nav navbar-nav right" >
                        <NavLinkLi to="#" icon="user-plus" title="Sample Link1" />
                        <NavLinkLi to="#" icon="sign-in" title="Sample Link2" />
                    </ul>
                </div>
            </div>
        );
    }
}

NavBarComponent.propTypes = {
    
}

function mapStateToProps(state){
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps, {  })(NavBarComponent) ;