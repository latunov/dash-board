import React, { Component, PropTypes } from 'react' ;
import CardComponent from './card.component';
import { DropTarget } from 'react-dnd';

class ListComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            listInfo: props.listInfo, 
            cards: props.cards,
            searchText: ''
        };
        this.cardsAll = [];
    }

    componentWillReceiveProps(nextProps){
        this.setState(nextProps);
        this.cardsAll = nextProps.cards;
    }
    
    reloadAllCards() {
        this.props.onReloadAllCards();
    }

    isDropped(card) {
        return false;
    }

    search(e, text){
        let searchText = '';
        if (e) searchText = e.target.value.toLowerCase();
        if (text) searchText = text;
        let cardsFound = [];
        if (searchText > '') {
            cardsFound = this.cardsAll.filter(i => (i.title.toLowerCase() + i.body.toLowerCase()).indexOf(searchText) > -1);
        } else {
            cardsFound = this.cardsAll;
        }
        this.setState({ cards: cardsFound, searchText });
    }

    render(){
        const cardsInLIst = this.state.cards.map((card,i)=>{
            return(
                <CardComponent 
                    cardInfo={card} 
                    key={i} 
                    isDropped={this.isDropped(card)}
                    onClick={this.props.onClick.bind(this, card.id)}
                />
            );
        });
        
        const { isOver, canDrop, connectDropTarget, lastDroppedItem } = this.props;
        
        return connectDropTarget(
            <div className="panel list-main panel-default">
                <div className="panel-heading">
                    <h2>{this.props.listInfo.name}</h2>
                </div>
                <div className="panel-search">
                    <i className="fa fa-search search-icon" aria-hidden="true"></i>
                    <input name="search" value={this.state.searchText} placeholder="Search..." onChange={this.search.bind(this)} />
                </div>
                <div className="panel-body">
                    <div className="list-group cards-list">
                        {cardsInLIst}
                    </div>
                </div>
            </div>
        );
    }
}
const types = '';

const listTarget = {
    drop(props, monitor) {
        props.onDrop(monitor.getItem(), props);
    },
    hover(props, monitor, component){
        const draggedId = monitor.getItem()._id;
    }
};

function collect(connect, monitor){
    return {
            connectDropTarget: connect.dropTarget(),
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
    }    
}

ListComponent.propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired,
    lastDroppedItem: PropTypes.object,
    onDrop: PropTypes.func.isRequired,
  };

export default DropTarget('Cards', listTarget, collect)(ListComponent);
