import React, { Component, PropTypes } from 'react' ;
import classnames from 'classnames';
import CardComponent from './card.component';

class CommentComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            listInfo: props.listInfo, 
            cards: props.cards,
            searchText: ''
        };
        this.cardsAll = [];
    }

    componentWillReceiveProps(nextProps){
        this.setState(nextProps);
        this.cardsAll = nextProps.cards;
    }
    
    reloadAllCards() {
        this.props.onReloadAllCards();
    }

    isDropped(card) {
        return false;
    }

    search(e, text){
        let searchText = '';
        if (e) searchText = e.target.value.toLowerCase();
        if (text) searchText = text;
        let cardsFound = [];
        if (searchText > '') {
            cardsFound = this.cardsAll.filter(i => (i.title.toLowerCase() + i.body.toLowerCase()).indexOf(searchText) > -1);
        } else {
            cardsFound = this.cardsAll;
        }
        this.setState({ cards: cardsFound, searchText });
    }

    render(){
        const cardsInLIst = this.state.cards.map((card,i)=>{
            return(
                <CardComponent 
                    cardInfo={card} 
                    key={i} 
                    isDropped={false}
                    onClick={() => {}}
                />
            );
        });
        
        return (
            <div className="panel list-main panel-default">
                <div className="panel-heading">
                    <h2>{this.props.listInfo.name}</h2>
                </div>
                <div className="panel-search">
                    <i className="fa fa-search search-icon" aria-hidden="true"></i>
                    <input name="search" value={this.state.searchText} placeholder="Search..." onChange={this.search.bind(this)} />
                </div>
                <div className="panel-body">
                    <div className="list-group cards-list">
                        {cardsInLIst}
                    </div>
                </div>
            </div>
        );
    }
}

export default CommentComponent;
