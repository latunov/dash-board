import React, { Component } from 'react' ;
import ListComponent from './list.component';
import CommentComponent from './comment.component';
import { listCards } from './../../data/api.service';
import { listLists } from './../../data/api.service';
import { listComments } from './../../data/api.service';
import { connect } from 'react-redux';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';

class ListsComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            boardId: props.boardId,
            addNewList: false,
            lists: [],
            cards: []
        };
    }

    componentDidMount() {
        this.loadLists();
    }

    loadLists() {
        listLists().then(
            res => {
                this.setState({ 
                    lists: res.lists,
                    cards: []
                });
                this.loadCards();
            },
            err => {
                console.log(err);
            }
        );
    }

    loadCards(){
        listCards()
        .then(
            res => {
                let data = res.data.splice(0, 10);
                this.setState({
                    cards: data,
                    lists: this.state.lists
                });
                const savedPostId = localStorage.getItem('selectedPostId');
                if (savedPostId) this.loadComments(savedPostId);
            },
            err => console.log(err)
        );
    }

    loadComments(postId){
        localStorage.setItem('selectedPostId', postId);
        listComments(postId)
        .then(
            res => {
                let data = res.data;
                data = data.map(e => {
                    e.title = e.name;
                    return e;
                });
                this.setState({
                    lists: this.state.lists,
                    cards: this.state.cards,
                    comments: data
                });
            },
            err => console.log(err)
        );
    }

    handleDrop(card, list) {
        let cards = this.state.cards;
        let cardd = cards.find(i => i._id == card._id);
        
        let cardToChange = Object.assign({}, cardd);
        cardToChange.listId = list.listInfo._id;

        updateCard(card._id, cardToChange)
        .then(
            res => {
                this.loadCards();
            },
            err => this.setState({ errors: err.response.data.errors })
        );
    }

    render(){
        return (
            <div className='b-main'>
                <div className='list-container'>
                    <ListComponent 
                        cards={this.state.cards}
                        listInfo={ {id: 1, name: 'Posts'} }
                        canDrop={true}
                        onDrop={this.handleDrop.bind(this)}
                        onClick={this.loadComments.bind(this)}
                    />
                </div>
                {this.state.comments && <div className='list-container'>
                    <CommentComponent 
                        cards={this.state.comments}
                        listInfo={ {id: 2, name: 'Comments'} }
                    />
                </div>}
            </div>            
        )
    }
}

// устанавливает листы для приема дропов
export default DragDropContext(HTML5Backend)(ListsComponent);
