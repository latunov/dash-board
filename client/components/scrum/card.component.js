import React, { Component, PropTypes } from 'react';
import { DragSource } from 'react-dnd';
import { Link } from 'react-router-dom';

class CardComponent extends Component{
    constructor(props){
        super(props);
        this.state = {
            cardInfo: props.cardInfo,
            editCard: false
        }
    }

    onEditCard(e){
        e.preventDefault();
        this.setState({
            editCard: !this.state.editCard
        })
    }

    onClick(e){
        e.preventDefault();
    }

    reloadCard(){
        this.props.onCardChanged();
        this.setState({
            editCard: false
        });
    }

    render(){
        const { isDropped, isDragging, connectDragSource } = this.props;

        return connectDragSource(
            <div style={{ opacity: isDragging ? 0.5 : 1 }} className="list-group-item card-href" onClick={this.props.onClick.bind(this)}>
                <div className='card-details'>
                    <h4 className="list-group-item-heading">{this.props.cardInfo.title}</h4>
                    <p className="card-desc">{this.props.cardInfo.body.split('\n').map((i, k) => <span key={k}>{i}<br /></span>)}</p>
                </div>
            </div>
        );
    }
}

const cardSource = {
    beginDrag(props) {
        return {
            _id: props.cardInfo._id,   
        };
    },
    endDrag(props, monitor, component){
        
    }
};

function collect(connect, monitor){
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }  
}

CardComponent.propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    isDropped: PropTypes.bool.isRequired,
};

export default DragSource('Cards', cardSource, collect)(CardComponent);