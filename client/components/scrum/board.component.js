import React, { Component } from 'react' ;
import ListsComponent from './lists.component';
import { connect } from 'react-redux'; 
import { setCurrentPost } from './../../actions/post.actions'; 

class BoardComponent extends Component{
    constructor(props){
        super(props);
    }
    
    render(){
        return (
            <div className='board-content'>
                <div className='board-content-wrap'>
                    <div className='board-content-full'>
                        <div className='b-header'>
                            <h2 className="appear left">Board</h2>
                        </div>
                        <div className='b-container'>
                            <ListsComponent />                            
                        </div>
                    </div>
                    <div className='board-content-menu hide'></div>
                </div>
            </div>
        )
    }
}
BoardComponent.propTypes = {
    setCurrentPost: React.PropTypes.func.isRequired
}

export default connect(null, { setCurrentPost })(BoardComponent)