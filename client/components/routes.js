import React from 'react'; 
import { Switch, Route , IndexRoute } from 'react-router' ;
import BoardComponent from './scrum/board.component' ;

const AppRoutes = () => (
  <Switch>
      <Route exact path='/' component={BoardComponent} />
  </Switch>
)

export default (
    <Route path='/' component={AppRoutes}> </Route>
)