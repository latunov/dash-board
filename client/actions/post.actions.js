import axios from 'axios';
import { SET_CURRENT_POST } from './../constants' ;

export function setCurrentPost(post){
    return {
        type: SET_CURRENT_POST, 
        post
    }
}
