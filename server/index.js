import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import config from './config' ;
import routesForApp from './routes';

let app = express();

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
routesForApp(app);

app.get('/*', (req, res)=> {
    res.sendFile(path.join(__dirname, '/public/index.html'));
})

app.listen(config.port, () => console.log('Listening on port ' + config.port + ' ...'));
